#! /bin/sh
exec guile-gtk -s $0 $*
!#

;;;;;;;;;;;;;;;;;;;;;;;;
;;Ariel Rios          ;;
;;jarios@usa.net      ;;
;;gtkhtml example     ;;
;;Mi na Nollag 12 1999;;
;;;;;;;;;;;;;;;;;;;;;;;;

;;Released under GPL;;
 
(use-modules (gtk gtk)
	     (gtk gdk)
             (gtk gtkhtml))

(load "txt2string.scm")

(gdk-rgb-init)

(gtk-widget-set-default-colormap (gdk-rgb-get-cmap))
(gtk-widget-set-default-visual (gdk-rgb-get-visual))

(define (test)
 
  (let* ((window (gtk-window-new 'toplevel))
	 (box (gtk-vbox-new #f 0))
	 (html (gtk-html-new))
	 (swin (gtk-scrolled-window-new #f #f))
	 (handle (gtk-html-begin html))
         (buffer (txt->string-file "prueba.html"))
	 (n (string-length buffer)))

	(gtk-scrolled-window-set-policy swin 'automatic 'automatic)
  
;    (gtk-widget-set-default-colormap (gdk-rgb-get-cmap))
;    (gtk-widget-set-default-visual (gdk-rgb-get-visual))

    (gtk-container-add swin html)
    ;(gtk-box-pack-start-defaults box html #t #t)
    (gtk-container-add window swin)

;    (gtk-widget-grab-focus html)

    ;(gtk-html-set-base-url html "http://www.gnome.org")

    (gtk-widget-realize html)
    (gtk-widget-set-usize window 500 400)

    (gtk-widget-show-all window)
   
 
    (gtk-html-write html handle buffer (string-length buffer))
    ;;(gtk-html-parse html)
    (gtk-html-end html handle 'ok) 
					;	(gtk-html-parse html)

	 (gtk-standalone-main window)
     ;(gtk-html-calc-scrollbars html)
  
   

))

(test)

