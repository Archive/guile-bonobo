(define-module (gtk libglade)
 :use-module (gtk dynlink))

(merge-compiled-code "libglade_init_glue" "libguileglade")
