(use-modules (gnome gnome)
	     (gtk gtk)
	     (gtk gdk))

(read-set! keywords 'prefix)

(gnome-init-hack "cauldron" #f '())

(define app (gnome-app-new "SART" "SART"))

(define caul
  (gtk-dialog-cauldron "Search" '(toplevel)
		       " ( %Ld | %Ed ) / %[ ( %Cd // %Cd // %Cd ) ]seo  / ( %Bqrxfp || %Bqxfp ) / ( %Xxf )"
		       app
		       "Enter search string:" "some default search string"
		       "Search options"
		       "Case sensitive" 1
		       "Whole words only" 0
		       "Regular expression" 0
		       "Ok"
		       "Cancel"
		       (lambda (window)
			 (gtk-button-new-with-label "Hello"))))
