(define-module (gnome gnomeprint)
  :use-module (gtk dynlink))

(merge-compiled-code "gnomeprint_init_glue" "libguilegnomeprint")
