# Generate the building cruft for a freshly checked out guile-gtk
# module.

##
## NOTE: before changing anything here, please read README.gnome-guile
##

aclocal -I . $ACLOCAL_FLAGS &&
libtoolize --copy --automake &&
autoheader &&
autoconf && 
automake --add-missing &&
(cd examples; aclocal $ACLOCAL_FLAGS; autoconf; automake) &&
echo Now run configure and make.
