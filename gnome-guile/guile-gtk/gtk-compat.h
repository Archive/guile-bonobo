#ifndef GTK_INTERP_H
#define GTK_INTERP_H

#include <gtk/gtk.h>

gfloat gtk_progress_bar_percentage (GtkProgressBar *bar);
gchar *gtk_label_get_interp (GtkLabel *label);
void gtk_menu_popup_interp (GtkMenu *menu,
			    GtkWidget *parent_menu_shell,
			    GtkWidget *parent_menu_item,
			    gint button,
			    guint32 activate_time);

GtkWidget* gtk_radio_menu_item_new_with_label_interp (GtkRadioMenuItem *group,
						      gchar            *label);
GtkWidget* gtk_radio_menu_item_new_interp (GtkRadioMenuItem *group);
GtkWidget* gtk_pixmap_new_interp (char *file, GtkWidget *intended_parent);

GdkColormap* gdk_colormap_ref   (GdkColormap *);
void         gdk_colormap_unref (GdkColormap *);
GdkVisual*   gdk_visual_ref     (GdkVisual *);
void         gdk_visual_unref   (GdkVisual *);
GdkWindow*   gdk_window_ref     (GdkWindow *);
void         gdk_window_unref   (GdkWindow *);
GdkEvent*    gdk_event_copy     (GdkEvent *);
void         gdk_event_free     (GdkEvent *);
GdkColor*    gdk_color_copy     (GdkColor *);
void         gdk_color_free     (GdkColor *);

GtkTooltips *gtk_tooltips_ref   (GtkTooltips *);
void         gtk_tooltips_unref (GtkTooltips *);

guint         gtk_timeout_add_interp (guint32            interval,
				     GtkCallbackMarshal function,
				     gpointer           data,
				     GtkDestroyNotify   notify);

guint         gtk_idle_add_interp   (GtkCallbackMarshal function,
				    gpointer           data,
				    GtkDestroyNotify   destroy);

guint        gtk_signal_connect_interp (GtkObject         *object,
					const gchar       *signal,
					GtkCallbackMarshal func,
					gpointer           data,
					GtkDestroyNotify   notify,
					gint               after);

GtkWidget *gtk_radio_button_new_interp            (GtkRadioButton *group);
GtkWidget *gtk_radio_button_new_with_label_interp (GtkRadioButton *group,
						   const gchar    *label);

GdkColor *gdk_color_parse_interp (char *spec);
GdkColor *gtk_style_get_white_interp (GtkStyle *style);

GdkColormap *gtk_widget_peek_colormap (void);

void gtk_list_append_item (GtkList *list, GtkListItem *item);

#if 0
gint gtk_input_add_interp (gint               source,
			   GdkInputCondition  condition,
			   GtkCallbackMarshal callback,
			   gpointer           data,
			   GtkDestroyNotify   destroy);
#endif

#ifdef HAVE_GTK_TEXT_FOREWARD_DELETE
/* Old versions have the incorrect spelling.  */
#define gtk_text_forward_delete gtk_text_foreward_delete
#endif

#endif
