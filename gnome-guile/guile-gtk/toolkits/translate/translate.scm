;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios               ;;
;; ariel@arcavia.com        ;;
;; http://erin.netpedia.net ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under GPL
;; Please Refer to the file COPYING

;; Programs for translating Gtk+ header files unto
;; guile-gtk defs file.

;; We need the ultrafamous case* so we can use it on strings

(load "case.scm")

(define regexp-enum    (make-regexp "typedef[\n ]+enum[{}A-z_,\n ]+;"))
(define regexp-func    (make-regexp "[A-z]+[ ]+[*_A-z]+[ ]+[(),*A-z]+;"))
(define regexp-object  (make-regexp "struct[\n ]+[_{};\n*A-z ]+;"))

(define regexp-list    (list regexp-enum regexp-func regexp-object)) 

(define (guile-gtk-translate . file)
  (if (null? file)
      (display "You need to give a Gtk C header file")
      (for-each guile-gtk-translate-single file)))

(define (guile-gtk-translate-single file)
  (let ((out (string-append file ".defs"))
	(in  (open-input-file  file)))
    (guile-gtk-parser out (read in))
    (for-each close  (list out in))))
 
(define (guile-gtk-parser-func func out ls)
  (if ls
      (for-each (lambda (v)
		  (for-each (lambda (x) (write-char (string->list (func (substring str (caar x) (caadr x)))) out)) v)) 
		(cadr ls))))

(define (guile-gtk-parser out str)
  (for-each (lambda (x y) (guile-gtk-parser-func x out y));;wrong
	    (list guile-gtk-grammar-enum guile-gtk-grammar-func guile-gtk-grammar-object)
	    (map (lambda (a) (regexp-exec a str)) (list regexp-enum regexp-func regexp-object))))

(define (guile-gtk-grammar-enum-val str)
  (letrec ((enum                         
	    (lambda (ls word param n)
	      (cond 
	       ((equal? #\} (car ls)) 
		(list (reverse (cons (apply string (reverse word)) param)) n))
	       ((char-whitespace? (car ls)) (enum (cdr ls) word param (1+ n)))
	       ((equal? #\, (car ls)) (enum (cdr ls) '() 
					    (cons (apply string (reverse word)) param) (1+ n))) 
	       (else (enum (cdr ls) (cons (string (car ls)) word) param (1+ n)))))))
    (enum (cdr (member #\{ (string->list str))) '() '() 0)))

(define (guile-gtk-grammar-enum-write-single param)
  (let ((ls (string->list param)))
    (string-append "   ("  
		   (list->string  
		    (map char-downcase (string->list (substring param (length (member #\_ (reverse ls))) 
								(length ls))))) 
		   " " param ")\n")))

(define (guile-gtk-grammar-enum-write param name)
  (string-append "(define-enum " name "\n"
		 (apply string-append (map guile-gtk-grammar-enum-write-single param)) ")\n"))

(define (guile-gtk-grammar-enum-name str)
  (let* ((ls (string->list str))     
	 (rl (reverse ls))
	 (i  (- (length ls) (1- (length (member #\_ ls)))))
	 (j  (1- (length (member #\} rl))))
	 (k  (1- (length (member #\{ rl)))))
    (substring str (if (> i k) (1+ j) i) (if (> i k)  (1- (length (member #\; rl))) k))))

;;  enum {
;;        GTK_HTML_STREAM_OK,
;;        GTK_HTML_STREAM_ERROR
;;   } GtkHTMLStreamStatus;

;; is turn into:
;;(define-enum GtkHtml
;;   (ok GTK_HTML_STREAM_OK)
;;   (error GTK_HTML_STREAM_ERROR)
;; )

(define (guile-gtk-grammar-enum str)
  (guile-gtk-grammar-enum-write (car (guile-gtk-grammar-enum-val str)) (guile-gtk-grammar-enum-name str)))

;; the *-grammar-val* funcs
(define (guile-gtk-grammar-val-trim str)
  (list->string (map (lambda (x) (case x ((#\*) #\space) ((#\_) #\-) (else x)))
		     (string->list str))))

(define (guile-gtk-grammar-val-param-guile ls)
  (map (lambda(x)  
	 (case* x 
		(("char"  "char *" "const gchar") "string")
		(("void" x) "")
		(("gint" "gint8" "gint16" "gint32") "int")
		(("guint" "guint8" "guint16" "guint32") "uint")
		(("double" "gdouble") "double")
		(("gboolean") "bool")
		(("glong") "long")
		(("gulong") "ulong")
		(("gfloat") "float")
 		(else x)))
       ls))

(define (guile-gtk-grammar-val-param-list str)
  (letrec ((val
	    (lambda (ls word obj param)
	      (cond
	       ((equal? #\) (car ls)) (reverse (cons (cons (apply string-append (reverse word)) obj) param)))
	       ((equal? #\, (car ls)) (val (cdr ls) '() '()
					   (cons (reverse (cons (apply string-append (reverse word)) obj)) param)))
	       (else (val (cdr ls) (cons (string (car ls)) word) obj param))))))
    (val (cdr (member #\( (string->list str))) '() '() '())))

(define (guile-gtk-grammar-val-param ls)
  (letrec ((split 
	    (lambda (ls out param)
	      (cond 
	       ((null? ls) (cdr (reverse (cons (apply string (reverse out)) (cons " " param)))))
	       ((char-whitespace? (car ls)) (split (cdr ls) '()
						   (cons (apply string (reverse out)) (cons " " param))))
	       (else (split (cdr ls) (cons (car ls) out) param))))))
    (map (lambda (x) (apply string-append x))
	 (map guile-gtk-grammar-val-param-guile
	      (map (lambda (x) (map guile-gtk-grammar-val-trim x))
		   (map  (lambda (x) (split (string->list (car x)) '() '())) ls) ))) ) )

(define (guile-gtk-grammar-val-str ls)
  (apply string-append (map (lambda (x) (string-append "(" x ")\n")) ls)))

(define (guile-gtk-grammar-val-types str)
  (guile-gtk-grammar-val-str (guile-gtk-grammar-val-param (guile-gtk-grammar-val-param-list str))))

(define (guile-gtk-grammar-val-func-return str)
  (let ((str (substring str 0 (- (string-length str) (length (member #\space (string->list str)))))))
    (guile-gtk-grammar-val-trim
     (case* str
	    (("char" "char *""const char" "const gchar") "string")
	    (("gint" "gint8" "gint16" "gint32") "int")
	    (("guint" "guint8" "guint16" "guint32") "uint")
	    (("double" "gdouble") "double")
	    (("gboolean") "bool")
	    (("glong") "long")
	    (("gulong") "ulong")
	    (("gfloat") "float")
	    (("void") "none")
	    (else str)))))
  
(define (guile-gtk-grammar-val-func-name str)
  (guile-gtk-grammar-val-trim
   (substring str (- (string-length str) (length (member #\space (string->list str))))
	      (- (string-length str) (length (member #\( (string->list str)))))))

(define (guile-gtk-grammar-func str)
  (string-append "(define-func" 
		 (guile-gtk-grammar-val-func-name str)   "\n  "           
		 (guile-gtk-grammar-val-func-return str) "\n  ("
		 (guile-gtk-grammar-val-types str)       "))\n" ))

;; GtkWidget *gtk_html_new       (void);
;; is turn into:
;;(define-func gtk_html_new
;; GtkWidget
;; ())

(define (guile-gtk-grammar-object-name str)
  (substring str (1+ (- (string-length str) (length (member #\_ (string->list str)))))
	     (- (string-length str) (length (member #\{  (string->list str))) 1)))

(define (guile-gtk-grammar-object-type-all str)
  (substring str (1+ (- (string-length str) (length (member #\{ (string->list str)))))
	     (- (string-length str) (length (member #\; (string->list str))) 1)))

(define (guile-gtk-grammar-object-type str)
  (apply string-append
	 (map (lambda (x) (if (char-whitespace? x) (string-append "") (string x)))
	      (string->list (substring (guile-gtk-grammar-object-type-all str) 0 
				       (- (string-length str) (length (member #\space (string->list str))) 1))))))

(define (guile-gtk-grammar-object str)
  (string-append "(define-object " (guile-gtk-grammar-object-name str) 
		 "(" (guile-gtk-grammar-object-type str) "))\n"))

;; The following structure:
;; struct _GtkBox
;; {
;;   GtkContainer container;
;;  
;;   GList *children;
;;  gint16 spacing;
;;  guint homogeneous : 1;
;; };
;; is turned into:
;; (define-object GtkBox (GtkContainer))






