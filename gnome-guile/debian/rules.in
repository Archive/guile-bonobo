#!/usr/bin/make -f

i=$(shell pwd)/debian/tmp
b=$(shell pwd)/debian/build

configure: configure.in
	@echo "--- Making configure script and configuring"
	chmod +x autogen.sh guile-gtk/autogen.sh
	./autogen.sh --prefix=/@PREFIX@ --with-gtk-prefix=/@GTKPREFIX@

Makefile: configure
	@echo "--- Configuring"
	./configure --prefix=/@PREFIX@ --with-gtk-prefix=/@GTKPREFIX@

build: configure Makefile build-debstamp
build-debstamp:
	@echo "--- Compiling"
	dh_testdir
	$(MAKE) all
	touch build-debstamp

clean: configure Makefile
	@echo "--- Cleaning"
	dh_testdir
	-rm -rf static shared
	-rm -f build-debstamp install-debstamp
	-make distclean
	-rm -f `find . -name "*~"`
	-rm -rf `find . -name "\.deps"`
	-rm -rf `find . -name "\.libs"`
	-rm -rf `find . -name "*\.rej"`
	-rm -rf debian/tmp `find debian/* -type d ! -name CVS` debian/files* core
	-rm -f debian/*substvars

install: build install-debstamp
install-debstamp:
	@echo "--- Installing"
	dh_testdir
	dh_testroot
	dh_clean
	rm -rf $(b)
	$(MAKE) install prefix=$(i)/@PREFIX@ exec_prefix=$(i)/@PREFIX@
	touch install-debstamp

install-save: install
	rm -rf $(i).saved
	cp -a $(i) $(i).saved

install-saved:
	rm -rf $(i)
	cp -a $(i).saved $(i)
	rm -rf $(b)
	touch install-debstamp

binary-indep: build install

binary-arch: build install \
		libguilegtk0@SUFFIX@ \
		libguilegtk-dev@SUFFIX@ \
		gnome-guile@SUFFIX@ 

#
# libguilegtk0@SUFFIX@
#

libguilegtk0@SUFFIX@: install
	@echo "--- Building: $@"
	dh_installdocs       -p$@ -P$(b)/$@ README NEWS AUTHORS
	dh_installchangelogs -p$@ -P$(b)/$@ ChangeLog
	dh_movefiles         -p$@ -P$(b)/$@
	dh_strip             -p$@ -P$(b)/$@ 
	dh_compress          -p$@ -P$(b)/$@ 
	dh_fixperms          -p$@ -P$(b)/$@ 
	dh_installdeb        -p$@ -P$(b)/$@
	dh_shlibdeps         -p$@ -P$(b)/$@
	dh_gencontrol        -p$@ -P$(b)/$@
	dh_makeshlibs        -p$@ -P$(b)/$@ -V
	dh_md5sums           -p$@ -P$(b)/$@
	dh_builddeb          -p$@ -P$(b)/$@
 
#
# libguilegtk-dev
#

libguilegtk-dev@SUFFIX@: install
	@echo "--- Building: $@"
	mkdir -p $(b)/$@/usr/doc
	cd $(b)/$@/usr/doc; ln -s libguilegtk0@SUFFIX@ $@	
	dh_movefiles         -p$@ -P$(b)/$@
	dh_strip             -p$@ -P$(b)/$@ 
	dh_compress          -p$@ -P$(b)/$@ 
	dh_fixperms          -p$@ -P$(b)/$@ 
	dh_installdeb        -p$@ -P$(b)/$@
	dh_shlibdeps         -p$@ -P$(b)/$@
	dh_gencontrol        -p$@ -P$(b)/$@
	dh_makeshlibs        -p$@ -P$(b)/$@ -V
	dh_md5sums           -p$@ -P$(b)/$@
	dh_builddeb          -p$@ -P$(b)/$@

#
# gnome-guile
#

gnome-guile@SUFFIX@: install
	@echo "--- Building: $@"
	mkdir -p $(b)/$@/usr/doc
	cd $(b)/$@/usr/doc; ln -s libguilegtk0@SUFFIX@ $@	
	dh_movefiles         -p$@ -P$(b)/$@
	dh_strip             -p$@ -P$(b)/$@ 
	dh_compress          -p$@ -P$(b)/$@ 
	dh_fixperms          -p$@ -P$(b)/$@ 
	dh_installdeb        -p$@ -P$(b)/$@
	dh_shlibdeps         -p$@ -P$(b)/$@
	dh_gencontrol        -p$@ -P$(b)/$@
	dh_undocumented      -p$@ -P$(b)/$@ \
				guile-gtk.1 \
				build-guile-gtk.1 \
				gnomeg.1
	dh_md5sums           -p$@ -P$(b)/$@
	dh_builddeb          -p$@ -P$(b)/$@

binary: binary-indep binary-arch
.PHONY: binary clean binary-indep binary-arch build install install-save install-saved


